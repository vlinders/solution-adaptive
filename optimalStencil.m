function [ an ] = optimalStencil( p, n, tau, u )
% newOp = OPTIMALSTENCIL(p,n,tau,u)
% p:    Formal order of accuracy is 2p
% n:    Stencil width is 2n+1
% tau:  Order of approximation of Taylor error is 2tau
% u:    Target column vector. If u is a matrix, each column of u is
%       interpreted as a separate variable.

% Create matrices V, W and Gamma
vand = vandermod(1:n,p);
V = vand(:,1:p);
W = vand(:,p+1:end);
VinvW = -round(V\W);
Gamma = [VinvW; eye(n-p)];

% Call conventional FD stencils of order 2p and 2tau
taucoeff = convFD(2*tau);
pcoeff   = convFD(2*p);
alpha    = taucoeff - [pcoeff; zeros(tau-p,1)];

% Construct matrix A and vector b
% (taking into account that u may have several columns)
[M,N] = size(u);
Amat = zeros(M,N,n-p);
for k=(p+1):n
    % Impose G_j u on the jth column of A
    Amat(:,:,k-p) = matMult(Gamma(:,k-p), u);
end
Bmat = matMult(alpha, u);
A = reshape(Amat,M*N,n-p);
b = Bmat(:);

% Solve least squares problems and construct an
ap2n = A\b;
an = [pcoeff + VinvW*ap2n; ap2n];
end

% Function to generate modified Vandermonde matrix
function A = vandermod(v, rows)
A = repmat(v, rows, 1);
A(2:end, :) = A(2:end, :).*A(2:end, :);
A = cumprod(A, 1);
end

% Function to call conventional finite difference coefficients
function g = convFD(order)
switch order
    case 0
        g = [];
    case 2
        g = 1/2;
    case 4
        g = [2/3; -1/12];
    case 6
        g = [3/4; -3/20; 1/60];
    case 8
        g = [4/5; -1/5; 4/105; -1/280];
    case 10
        g = [5/6; -5/21; 5/84; -5/504; 1/1260];
    case 12
        g = [6/7; -15/56; 5/63; -1/56; 1/385; -1/5544];
    case 14
        g = [7/8; -7/24; 7/72; -7/264; 7/1320; -7/10296; 1/24024];
    case 16
        g = [8/9; -14/45; 56/495; -7/198; 56/6435; -2/1287; 8/45045; -1/102960];
end
end

function [ vout ] = matMult( c, vin )
% MATMULT Skew-symmetric circulant matrix operation
% vout = MATMULT(c,vin) returns the matrix-vector product C*f,
% where C is the skew-symmetric circulant matrix formed by shifting the 
% row vector [-fliplr(c) 0 c] down the diagonal.

vout = zeros(size(vin));
for k=find(c(:)')
    vout = vout + c(k)*( circshift(vin,-k) - circshift(vin,k) );
end
end