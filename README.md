# README #

This repository contains an implementation of Algorithm 1 from "Accurate Solution-Adaptive Finite Difference Schemes for Coarse and Fine Grids" by V. Linders, M.H. Carpenter and J. Nordström.